# Liste des scripts 

## 1.1 : liste des images sans alt

- Désactive les CSS
- Dans le titre de la page, annonce le nombre d'images et le nombres d'images sans alt
- En haut de page, liste les sources des images qui n'ont pas de alt

## 2.1, 2.2 : auditer les iframe (présence et pertinence)

- Désactive les CSS
- Dans le titre de la page, annonce le nombre de cadres  et le nombres de cadres sans titres
- En haut de page, liste les titres des cadres

## 8.2 : Valider le code source

- Ouvre le validateur HTML dans une nouvelle fen^etre avec l'url du site

## 9.1 : auditer les titres et la hiérarchie

- Désactive les CSS
- Dans le titre de la page, annonce s'il manque un titre h1 et annonce le nombre de titres dans la page
- En haut de page, liste les tires et leur niveau
- Dans le corps de la page, ajotue h1, h2 etc... devant les titres

