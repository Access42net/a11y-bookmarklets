// ==UserScript==
// @name        5.6. Taleaux
// @updateURL	https://access42net.gitlab.io/a11y-bookmarklets/src/5.6.th-table.user.js
// @downloadURL	https://access42net.gitlab.io/a11y-bookmarklets/src/5.6.th-table.user.js
// @version      0.1
// @description  Détecte les ent^etes de tableau
// @author       Access42
// @grant        none
// @include		http://*
// @include		https://*
// @include		*
// @require		https://access42net.gitlab.io/a11y-bookmarklets/src/css.reset.js
// ==/UserScript==

(function() {
    'use strict';
	var allTable = document.getElementsByTagName("table");
	var allTableHeadings = document.getElementsByTagName("th");

	var resultBlock = document.createElement('div');
	resultBlock.setAttribute('role','alert');
	resultBlock.setAttribute('tabindex','-1');
	resultBlock.setAttribute('aria-label','Critère 5.6 - Liste des tableaux');


	for(var t=0 ; t<allTable.length ; t++){
		if(allTable[t].getAttribute('role') == 'presentation'){
			for(var i=0; i<allTableHeadings.length;i++){
				var content = document.createElement('span');
				content.innerHTML = ('&lt;th&gt;');
				if(allTableHeadings[i].getAttribute('scope')){
					content.innerHTML = ('&lt;th scope="' + allTableHeadings[i].getAttribute('scope') + '"&gt;');
				}
				allTableHeadings[i].prepend(content);
			}
		}
	}

})();

/**
var	iframeCounter = document.createElement('p');
iframeCounter.innerHTML = allIframe.length + ' cadre(s) dans la page';
resultBlock.appendChild(iframeCounter);
resultBlock.appendChild(iframeList);
document.title = allIframe.length + ' cadre(s) dans la page ' + iframeTitlelessCounter + document.title;

document.body.appendChild(resultBlock);

for(var i=0; i<allIframe.length;i++){
	allIframe[i].setAttribute('style','border:2px solid red');
}

resultBlock.focus();**/