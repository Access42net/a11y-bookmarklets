// ==UserScript==
// @name         0.Désactiver CSS
// @updateURL	https://access42net.gitlab.io/a11y-bookmarklets/src/0.disable-css.user.js
// @downloadURL	https://access42net.gitlab.io/a11y-bookmarklets/src/0.disable-css.user.js
// @version      0.1
// @description  Désactive les CSS
// @author       Access42
// @grant        none
// @include   http://*
// @include   https://*
// @include   *
// @require   https://access42net.gitlab.io/a11y-bookmarklets/src/css.reset.js
// ==/UserScript==

(function() {

})();