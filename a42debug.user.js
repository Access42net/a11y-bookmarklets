// ==UserScript==
// @name        Test full debug
// @updateURL	https://access42net.gitlab.io/a11y-bookmarklets/a42debug.user.js
// @downloadURL	https://access42net.gitlab.io/a11y-bookmarklets/a42debug.user.js
// @version      0.1
// @description  Debug a11y - RGAA
// @author       Access42
// @grant        none
// @include		http://*
// @include		https://*
// @include		*
// ==/UserScript==

	'use strict';

	var btnGp = document.createElement('div');
	btnGp.setAttribute('id','btnGp-a42-debug');
	btnGp.setAttribute('style','position:absolute;top:0;width:100%;z-index:9999;background:#ffff;padding:0.5em;border-bottom:1px solid #000');
	document.body.prepend(btnGp);

	var btnTestHeadings = document.createElement('button');
	btnTestHeadings.textContent = '9.1. titres et hiérarchie';
	btnTestHeadings.onclick = testHeadings;
	btnGp.prepend(btnTestHeadings);

	var btnTestCode = document.createElement('button');
	btnTestCode.textContent = '8.2. Valider le code source';
	btnTestCode.onclick = testCode;
	btnGp.prepend(btnTestCode);

	var btnLinks = document.createElement('button');
	btnLinks.textContent = '6. Liens';
	btnLinks.onclick = testLinks;
	btnGp.prepend(btnLinks);

	var btnTestIframe = document.createElement('button');
	btnTestIframe.textContent = '2.Cadres';
	btnTestIframe.onclick = testIframe;
	btnGp.prepend(btnTestIframe);

	var btnAltImages = document.createElement('button');
	btnAltImages.textContent = '1.1. Alt images';
	btnAltImages.onclick = testAltImages;
	btnGp.prepend(btnAltImages);

//reset css - credit CSS-tricks : https://css-tricks.com/snippets/javascript/remove-inline-styles/
function remove_style(all) {
	var i = all.length;
	var j, is_hidden;
	// Presentational attributes.
	var attr = [
		'align',
		'background',
		'bgcolor',
		'border',
		'cellpadding',
		'cellspacing',
		'color',
		'face',
		'height',
		'hspace',
		'marginheight',
		'marginwidth',
		'noshade',
		'nowrap',
		'valign',
		'vspace',
		'width',
		'vlink',
		'alink',
		'text',
		'link',
		'frame',
		'frameborder',
		'clear',
		'scrolling',
		'style'
	];

	var attr_len = attr.length;

	while (i--) {
		is_hidden = (all[i].style.display === 'none');

		j = attr_len;

    while (j--) {
      all[i].removeAttribute(attr[j]);
    }
  }
  
  var allCSS = document.querySelectorAll('link[rel="stylesheet"],style');
  for(var i=0; i<allCSS.length;i++){
    allCSS[i].remove();
  }
}

//1.1		
function testAltImages() {
    'use strict';

   if(document.getElementById('a42-debug-mode')){
      document.getElementById('a42-debug-mode').remove();
    }

    var all = document.getElementsByTagName('*');
    remove_style(all);

    var allImages = document.querySelectorAll("img, input[type='image'],area");

    var resultBlock = document.createElement('div');
    resultBlock.setAttribute('id','a42-debug-mode');
    resultBlock.setAttribute('role','alert');
    resultBlock.setAttribute('tabindex','-1');
    resultBlock.setAttribute('aria-label','Critère 1.1 - Images sans alternative');
    document.body.prepend(resultBlock);

    var imagesList = document.createElement('ul');

    var imagesNoAltCounter = 0;

    for(var i=0; i<allImages.length;i++){

        if(allImages[i].getAttribute('alt')==null){

            var content = document.createElement('li');
            content.innerHTML = (allImages[i].tagName+" src='"+allImages[i].src)+"'";
            imagesList.appendChild(content);
            allImages[i].setAttribute('style','border:2px solid red');
            imagesNoAltCounter++;

        }

    }

    var	imagesCounter = document.createElement('p');
    imagesCounter.innerHTML = allImages.length + ' images(s) dans la page dont ' + imagesNoAltCounter + ' sans alt ';
    resultBlock.appendChild(imagesCounter);
    resultBlock.appendChild(imagesList);

    document.title = allImages.length + ' image(s) dans la page dont ' + imagesNoAltCounter + ' sans alt ';

    resultBlock.focus();
}
//2.1
function testIframe() {
    'use strict';

	if(document.getElementById('a42-debug-mode')){
		document.getElementById('a42-debug-mode').remove();
	}

    var all = document.getElementsByTagName('*');
    remove_style(all);
	
	var allIframe = document.getElementsByTagName("iframe");

	var resultBlock = document.createElement('div');
    resultBlock.setAttribute('id','a42-debug-mode');
	resultBlock.setAttribute('role','alert');
	resultBlock.setAttribute('tabindex','-1');
	resultBlock.setAttribute('aria-label','Critères 2.1, 2.2 - Liste des cadres et de leurs titres');
	document.body.prepend(resultBlock);


	var iframeList = document.createElement('ul');
	var iframeTitlelessCounter = 0;
	for(var i=0; i<allIframe.length;i++){

		var content = document.createElement('li');
		content.innerHTML = (allIframe[i].getAttribute('title'));

		if(allIframe[i].getAttribute('title')==null){
			content.innerHTML = 'Cadre sans attribut title ';
			iframeTitlelessCounter++;
		}

		iframeList.appendChild(content);

	}

	if(iframeTitlelessCounter){
		iframeTitlelessCounter = 'dont ' + iframeTitlelessCounter + ' cadre(s) sans title ';
	}else{
		iframeTitlelessCounter = '';
	}

	var	iframeCounter = document.createElement('p');
	iframeCounter.innerHTML = allIframe.length + ' cadre(s) dans la page';
	resultBlock.appendChild(iframeCounter);
	resultBlock.appendChild(iframeList);
	document.title = allIframe.length + ' cadre(s) dans la page ' + iframeTitlelessCounter;


	for(var i=0; i<allIframe.length;i++){
		allIframe[i].setAttribute('style','border:2px solid red');
	}

	resultBlock.focus();
}
//8.2
function testCode() {
    'use strict';

    if(document.getElementById('a42-debug-mode')){
        document.getElementById('a42-debug-mode').remove();
    }

    void(window.open('https://validator.w3.org/nu/?doc='+encodeURIComponent(location.href)),'_blank');
}
//9.1
function testHeadings() {
    'use strict';

	if(document.getElementById('a42-debug-mode')){
		document.getElementById('a42-debug-mode').remove();
	}

    var all = document.getElementsByTagName('*');
    remove_style(all);

	var allHeadings = document.querySelectorAll("h1, h2, h3, h4, h5, h6, [role='heading']");

	var resultBlock = document.createElement('div');
    resultBlock.setAttribute('id','a42-debug-mode');
	resultBlock.setAttribute('role','alert');
	resultBlock.setAttribute('tabindex','-1');
	resultBlock.setAttribute('aria-label','Crit&egrave;re 9.1 - Liste des titres pr&eacute;sents dans la page');
	document.body.prepend(resultBlock);

	var headingsList = document.createElement('ul');

	for(var i=0; i<allHeadings.length;i++){

		var content = document.createElement('li');
		content.innerHTML = (allHeadings[i].tagName+" : "+allHeadings[i].textContent);

		if((allHeadings[i].getAttribute('aria-level'))&&(allHeadings[i].getAttribute('role'))){
			content.innerHTML = (allHeadings[i].tagName + " role='" + allHeadings[i].getAttribute('role') + "' aria-level='" + allHeadings[i].getAttribute('aria-level') + "' : " + allHeadings[i].textContent); 
		}

		headingsList.appendChild(content);

	}

	var	headingsCounter = document.createElement('p');
	headingsCounter.innerHTML = allHeadings.length + ' titre(s) dans la page';
	resultBlock.appendChild(headingsCounter);
	resultBlock.appendChild(headingsList);
    if(document.querySelectorAll('h1,[aria-level="1"]').length==0){
        var noh1 = document.createElement('p');
        noh1.innerHTML = 'Il manque un titre h1';
        resultBlock.appendChild(noh1);
    }
    if(document.querySelectorAll('h1,[aria-level="1"]').length==0){
        document.title = 'Il manque un titre h1 - Il y a ' + allHeadings.length + ' titre(s) dans la page ';
    }else{
    document.title = allHeadings.length + ' titre(s) dans la page ';}

	for(var i=0; i<allHeadings.length;i++){
		allHeadings[i].setAttribute('style','border:2px solid red');
		allHeadings[i].prepend(allHeadings[i].tagName+" ");
		if((allHeadings[i].getAttribute('aria-level'))&&(allHeadings[i].getAttribute('role'))){
			allHeadings[i].prepend(allHeadings[i].tagName + " role='" + allHeadings[i].getAttribute('role') + "' aria-level='" + allHeadings[i].getAttribute('aria-level') + "' ");
		}
	}

	resultBlock.focus();
}
//6.*
function testLinks() {
    'use strict';

	if(document.getElementById('a42-debug-mode')){
		document.getElementById('a42-debug-mode').remove();
	}

    var all = document.getElementsByTagName('*');
    remove_style(all);

	var allLinks = document.querySelectorAll("a[href], [role='link']");

	var resultBlock = document.createElement('div');
    resultBlock.setAttribute('id','a42-debug-mode');
	resultBlock.setAttribute('role','alert');
	resultBlock.setAttribute('tabindex','-1');
	resultBlock.setAttribute('aria-label','Thématique 6 - Liste des liens pr&eacute;sents dans la page');
	document.body.prepend(resultBlock);

	var linksList = document.createElement('table');
	linksList.setAttribute('style','border:1px solid black');
	var linksListCaption = document.createElement('caption');
	linksListCaption.innerHTML = 'Liste des liens présents dans la page';
	linksList.appendChild(linksListCaption);

	var linksListHeadingsGroup = document.createElement('tr');
	linksListHeadingsGroup.setAttribute('style','border:1px solid black');
	linksList.appendChild(linksListHeadingsGroup);

	var linksListHeading1 = document.createElement('th');
	linksListHeading1.innerHTML='intitulé';
	linksListHeadingsGroup.appendChild(linksListHeading1);

	var linksListHeading2 = document.createElement('th');
	linksListHeading2.innerHTML='title';
	linksListHeadingsGroup.appendChild(linksListHeading2);

	var linksListHeading3 = document.createElement('th');
	linksListHeading3.innerHTML='nouvelle fen&ecirc;tre';
	linksListHeadingsGroup.appendChild(linksListHeading3);

	for(var i=0; i<allLinks.length;i++){

		var content = document.createElement('tr');
		content.setAttribute('style','border:1px solid black');

		var linkText = document.createElement('td');

//BUG //
		var childLinks = allLinks[i].childNodes;
		if(allLinks[i].childNodes){
			for(var e = 0 ; e < childLinks.length ; e++){
				linkText.innerHTML = childLinks[e].tagName;
			}
		}else{
			linkText.innerHTML = allLinks[i].textContent;			
		}
		linkText.innerHTML = allLinks[i].innerText;	
		content.appendChild(linkText);

		var linkTitle = document.createElement('td');
		if(allLinks[i].getAttribute('title')){
			linkTitle.innerHTML = allLinks[i].getAttribute('title');
		}else{
			linkTitle.innerHTML = '';
		}
		content.appendChild(linkTitle);

		var linkOpen = document.createElement('td');
		if(allLinks[i].getAttribute('target')){
			if((allLinks[i].getAttribute('target').value != '_self') || (allLinks[i].getAttribute('target').value != '_parent')){
				linkOpen.innerHTML = 'nouvelle fen&ecirc;tre';
			}
		}else{
				linkOpen.innerHTML = '';
			}
		content.appendChild(linkOpen);

		linksList.appendChild(content);

	}


	var	linksCounter = document.createElement('p');
	linksCounter.innerHTML = allLinks.length + ' lien(s) dans la page';
	resultBlock.appendChild(linksCounter);
	resultBlock.appendChild(linksList);
    
    if(document.querySelectorAll('a[href],[role="link"]').length==0){
        document.title = 'Pas de liens dans la page ';
    }else{
    document.title = allLinks.length + ' lien(s) dans la page ';}

	for(var i=0; i<allLinks.length;i++){
		allLinks[i].setAttribute('style','border:2px solid red');
		allLinks[i].prepend(allLinks[i].tagName+" ");
		if(allLinks[i].getAttribute('role')){
			allLinks[i].prepend(allLinks[i].tagName + " role='" + allLinks[i].getAttribute('role') + "'");
		}
	}

	resultBlock.focus();
}
